package com.svi.pdfpagecounter.csvparser;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class CsvWriter {
	public void appendData(String reportsFilePath, String data) {
		File report = new File(reportsFilePath);
		try {
			FileWriter masterlist = new FileWriter(report, true);
			masterlist.append(String.valueOf(data));
			masterlist.append("\n");
			masterlist.flush();
			masterlist.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void appendHeader(String reportsFilePath) {
		File report = new File(reportsFilePath);
		try {
			FileWriter masterlist = new FileWriter(report, true);
			masterlist.append("\"branch_code\",\"date_transmitted\",\"batch_number\",\"num_of_accounts\",\"page_count\"");
			masterlist.append("\n");
			masterlist.flush();
			masterlist.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		
	}
}
