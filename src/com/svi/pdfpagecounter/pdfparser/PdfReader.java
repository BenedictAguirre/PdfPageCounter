package com.svi.pdfpagecounter.pdfparser;

import java.io.File;
import java.io.IOException;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;

public class PdfReader {
	public String getPdfTextContent(String pdfFilePath) {
		String text = "";
		File file = new File(pdfFilePath);
	      PDDocument document;
		try {
			document = PDDocument.load(file);
			 //Instantiate PDFTextStripper class
		      PDFTextStripper pdfStripper = new PDFTextStripper();

		      //Retrieving text from PDF document
		      text = pdfStripper.getText(document);
		      System.out.println(text);
		      document.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return text;
	}
	public int getPdfPageCount(String pdfFilePath) {
		int count = 0;
		File file = new File(pdfFilePath);
	      PDDocument document;
		try {
			document = PDDocument.load(file);
			 count = document.getNumberOfPages();
		      document.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return count;
	}
}
