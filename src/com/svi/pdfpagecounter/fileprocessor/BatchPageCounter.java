package com.svi.pdfpagecounter.fileprocessor;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import com.svi.pdfpagecounter.config.Config;
import com.svi.pdfpagecounter.csvparser.CsvWriter;
import com.svi.pdfpagecounter.pdfparser.PdfReader;

public class BatchPageCounter {
	CsvWriter writer = new CsvWriter();

	private String getCurrentDate() {
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMdd");
		LocalDateTime now = LocalDateTime.now();
		return dtf.format(now);
	}

	private static final String REPORTS_DIRECTORY = Config.getProperty("reports_file");

	private File[] getFiles(String sourceFolder) {
		File directoryPath = new File(sourceFolder);
		File[] filesList = directoryPath.listFiles();
		return filesList;
	}

	public int countPagesPerBatch(String sourceFolder) {
		int countPerBatch = 0;
		BatchPageCounter c = new BatchPageCounter();
		PdfReader p = new PdfReader();
		File[] fl = c.getFiles(sourceFolder);
		for (File file : fl) {
			if (file.getName().endsWith(".pdf") && !file.getName().contains("Upload Acknowledgement Report")) {
				countPerBatch += p.getPdfPageCount(file.getAbsolutePath());
			}
		}
		return countPerBatch;
	}

	public String buildReportData(String branchCode, String date, String batchNo, String noOfAccounts,
			String sourceFolder) {
		String data = "";
		int pageCount = countPagesPerBatch(sourceFolder);
		data = branchCode + ",\"" + date + "\",\"" + batchNo + "\",\"" + noOfAccounts + "\",\"" + pageCount + "\"";
		return data;
	}

	private File createDirectory() {
		File reportsDir = new File(REPORTS_DIRECTORY);
		boolean result;
		try {
			result = reportsDir.mkdir(); 
			if (result)
			{
				System.out.println("Directory created: " + reportsDir.getCanonicalPath());
			} else {
				System.out.println("File already exists: " + reportsDir.getCanonicalPath());
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return reportsDir;
	}

	private File createTodaysReportFile(File reportDir) {
		String reportFilePath = reportDir.getAbsolutePath() + "/" + getCurrentDate() + ".csv";
		File myObj = new File(reportFilePath);
		
		try {
			if (myObj.createNewFile()) {
				writer.appendHeader(myObj.getAbsolutePath());
				System.out.println("File created: " + myObj.getAbsolutePath());
			} else {
				System.out.println("File already exists." + myObj.getAbsolutePath());
			}
		} catch (IOException e) {
			System.out.println("An error occurred.");
			e.printStackTrace();
		}
		return myObj;

	}

	public void writeReport(String branchCode, String date, String batchNo, String noOfAccounts, String sourceFolder) {

		String data = buildReportData(branchCode, date, batchNo, noOfAccounts, sourceFolder);
		File reportDir = createDirectory();
		File reportFile = createTodaysReportFile(reportDir);
		writer.appendData(reportFile.getAbsolutePath(), data);
		System.out.println(data + " successfully added to " + reportFile.getAbsolutePath());
	}

	public static void main(String[] args) {

		BatchPageCounter c = new BatchPageCounter();
		String branchCode = args[0];
		String date = args[1];
		String batchNo = args[2];
		String noOfAccounts = args[3];
		String sourceFolder = args[4];

		c.writeReport(branchCode, date, batchNo, noOfAccounts, sourceFolder);
		;
	}
//	"330", "081420", "B0001", "resources/330/081420_B0001"
}
